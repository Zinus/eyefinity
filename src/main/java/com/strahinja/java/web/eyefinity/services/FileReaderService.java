package com.strahinja.java.web.eyefinity.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.stereotype.Service;

@Service
public class FileReaderService {

	
	public String readTxtFile(File file) throws IOException {
		
		if(file == null)
			return null;
		
		String returnString;
		BufferedReader br = new BufferedReader(new FileReader(file));
		try {
		     StringBuilder sb = new StringBuilder();
		     String line = br.readLine();
	
		     while (line != null) {
		         sb.append(line);
		         sb.append(System.lineSeparator());
		         line = br.readLine();
		     }
		     returnString = sb.toString();
		 } finally {
		     br.close();
		 }
		
		return returnString;
	}
	
}
