package com.strahinja.java.web.eyefinity.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorPageController {
	@RequestMapping("/errorPage")
	public String getBody(Model model) {
		
		return "errorPage";
	}
	
}
