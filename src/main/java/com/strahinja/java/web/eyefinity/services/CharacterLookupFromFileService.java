package com.strahinja.java.web.eyefinity.services;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CharacterLookupFromFileService {

	@Autowired
	FileConverterService fileConverterService;
	
	@Autowired
	FileReaderService fileReaderService;
	
	@Autowired
	CharacterLookupService characterLookupService;
	
	
	public int countSpecificCharacterfromFile(MultipartFile multipartFile) throws IOException {
		String text = null;
		
		File file = fileConverterService.convertMultipartToFile(multipartFile);
		text = fileReaderService.readTxtFile(file);
		file.delete();
		
		int result = characterLookupService.countSpecificCharacter(text, 'i');
		
		return result;
	}

}
