package com.strahinja.java.web.eyefinity.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class FileConverterService {

	private final Path rootLocation;

    @Autowired
    public FileConverterService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    public File convertMultipartToFile(MultipartFile multipartFile) throws IOException {
		
		if(multipartFile == null)
			return null;
		
		File convFile = new File(multipartFile.getOriginalFilename());
		convFile.createNewFile(); 
		FileOutputStream fos = new FileOutputStream(convFile); 
		fos.write(multipartFile.getBytes());
		fos.close(); 
		
	
		return convFile;
	}

    public void init() throws IOException {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new IOException("Could not initialize storage", e);
        }
    }
}
