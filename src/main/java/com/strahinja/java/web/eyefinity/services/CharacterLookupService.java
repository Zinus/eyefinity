package com.strahinja.java.web.eyefinity.services;

import org.springframework.stereotype.Service;

@Service
public class CharacterLookupService {

	public int countSpecificCharacter(String text, char character) {
		int result = 0;
		
		if(text == null)
			return 0;
		
		text = text.toLowerCase();
		character = String.valueOf(character).toLowerCase().toCharArray()[0]; 
		
		for(int i=0;i<text.length();i++) {
			if(text.charAt(i)==character) {
				result++;
			}
		}
		
		return result;
	}
	
}
