package com.strahinja.java.web.eyefinity;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.strahinja.java.web.eyefinity.services.FileConverterService;
import com.strahinja.java.web.eyefinity.services.StorageProperties;


@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class Application 
{
    
	public static void main(String[] args) {
		SpringApplication.run(Application.class,args);
	}
    
	@Bean
    CommandLineRunner init(final FileConverterService fileConverterService) {
        return (args) -> {
            fileConverterService.init();
        };
    }
}
