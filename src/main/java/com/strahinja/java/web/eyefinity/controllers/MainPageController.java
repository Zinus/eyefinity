package com.strahinja.java.web.eyefinity.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.strahinja.java.web.eyefinity.services.CharacterLookupFromFileService;
import com.strahinja.java.web.eyefinity.services.CharacterLookupService;

@Controller
public class MainPageController {

	@Autowired
	CharacterLookupService characterLookupService;
	
	@Autowired
	CharacterLookupFromFileService advancedCharacterLookupService;
	
	
	@RequestMapping("/")
	public String getBody(Model model) {
		
		return "index";
	}
	
	@PostMapping("/plainText")
	public String useService(@RequestParam("plainTextValue") String plainTextValue, Model model, RedirectAttributes redirectAttributes) {
		int result = characterLookupService.countSpecificCharacter(plainTextValue, 'i');
		model.addAttribute("result", result);
		return "forward:/result";
	}
	
	@PostMapping("/multipartFile")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes, Model model) {

        int result;
		try {
			result = advancedCharacterLookupService.countSpecificCharacterfromFile(file);
		} catch (IOException e) {
			model.addAttribute("ErrorMessage","Something went wrong with your file upload :(");
			return "forward:/errorPage";
		}

        model.addAttribute("result", result);
        return "forward:/result";
    }
}
