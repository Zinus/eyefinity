package com.strahinja.java.web.eyefinity.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ResultPageController {

	@RequestMapping("/result")
	public String getBody(Model model) {
		
		return "result";
	}
	
}
