package com.strahinja.java.web.eyefinity.services;

import org.junit.Test;


import junit.framework.TestCase;

public class CharacterLookupServiceTest extends TestCase{

	@Test
	public void testGetsNullShouldReturn0() {
		CharacterLookupService characterLookupService = new CharacterLookupService();
		assertEquals(0,characterLookupService.countSpecificCharacter(null, 'i'));
	}
	
	@Test
	public void testGetsEmptyStringShouldReturn0() {
		CharacterLookupService characterLookupService = new CharacterLookupService();
		assertEquals(0,characterLookupService.countSpecificCharacter("", 'i'));
	}
	
	
	@Test
	public void testGetsStringShouldReturn0() {
		CharacterLookupService characterLookupService = new CharacterLookupService();
		//string doesn't contain character i
		assertEquals(0,characterLookupService.countSpecificCharacter("abcdefg", 'i'));
	}
	
	@Test
	public void testGetsStringShouldReturn1() {
		CharacterLookupService characterLookupService = new CharacterLookupService();
		//string contains one character i
		assertEquals(1,characterLookupService.countSpecificCharacter("abcdefgi", 'i'));
	}
	
	@Test
	public void testGetsStringShouldReturn2() {
		CharacterLookupService characterLookupService = new CharacterLookupService();
		//string contains upper and lower case i
		assertEquals(2,characterLookupService.countSpecificCharacter("lower case i upper case I", 'i'));
	}
}
