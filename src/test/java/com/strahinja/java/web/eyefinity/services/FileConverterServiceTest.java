package com.strahinja.java.web.eyefinity.services;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.apache.commons.io.FileUtils;

public class FileConverterServiceTest {

	//passing null as parameter should return null
	@Test
	public void testGetsNullShouldReturnNull() {
		StorageProperties properties = new StorageProperties();
		FileConverterService fileConverterService = new FileConverterService(properties);
		
		try {
			assertNull(fileConverterService.convertMultipartToFile(null));
		} catch (IOException e) {
			assertTrue(false);
		}
		
	}
	
	//test case for file conversion
	@Test
	public void testGetsMultipartShouldReturnFile() {
		StorageProperties properties = new StorageProperties();
		FileConverterService fileConverterService = new FileConverterService(properties);

		File file = new File("src/test/resources/test_random_no_i.txt");
		MockMultipartFile multipartFile = new MockMultipartFile(file.getName(), file.getName(), null, "abcdefghjk\r\nlmnopqrstu".getBytes());
		
		try {
			File file2 = fileConverterService.convertMultipartToFile(multipartFile);
			boolean flag = FileUtils.contentEquals(file, file2);
			file2.delete();
			assertTrue(flag);
		} catch (IOException e) {
			assertTrue(false);
		}
		
	}

	
	
}
