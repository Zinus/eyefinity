package com.strahinja.java.web.eyefinity.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MainPageControllerTest {

	private MockMvc mvc;
	
	@Autowired
	WebApplicationContext webApplicationContext;
	
	@Autowired
	private MainPageController mainPageController;
	
	@Before
	public void setup() {
		 mvc = MockMvcBuilders.standaloneSetup(mainPageController)
	                .build();
	}
	
	@Test
	public void testShouldReturnNotNull() throws Exception {
	    assertThat(mainPageController).isNotNull();
	}
	
	@Test
	public void testShouldReturnStatusIsOk() {
		
		MockHttpServletResponse response;
		try {
			response = mvc.perform(
			        get("/").accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();
			assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		} catch (Exception e) {
			assertTrue(false);
		}
		
	}
	
	@Test
	public void testPostPlainTextShouldReturnStatusIsOk() {
		
		MockHttpServletResponse response;
		try {
			response = mvc.perform(
			        post("/plainText").param("plainTextValue", "").accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();
			assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		} catch (Exception e) {
			assertTrue(false);
		}
		
	}
	
	@Ignore
	@Test
	public void testPostFileUploadShouldReturnStatusIsOK() throws Exception {
		
		 MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some data".getBytes());
	        
	        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	        mockMvc.perform(MockMvcRequestBuilders.multipart("/multipart")
	                        .file(firstFile)
	                        .param("file", "4"))
	                    .andExpect(status().is(200));
		
	}
	
}
