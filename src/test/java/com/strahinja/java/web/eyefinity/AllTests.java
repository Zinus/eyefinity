package com.strahinja.java.web.eyefinity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.strahinja.java.web.eyefinity.controllers.ErrorPageControllerTest;
import com.strahinja.java.web.eyefinity.controllers.MainPageControllerTest;
import com.strahinja.java.web.eyefinity.controllers.ResultPageControllerTest;
import com.strahinja.java.web.eyefinity.services.CharacterLookupServiceTest;
import com.strahinja.java.web.eyefinity.services.FileConverterServiceTest;
import com.strahinja.java.web.eyefinity.services.FileReaderServiceTest;

@RunWith(Suite.class)
@SuiteClasses({ 
	ApplicationTest.class, 
	CharacterLookupServiceTest.class, 
	FileReaderServiceTest.class, 
	FileConverterServiceTest.class,
	ErrorPageControllerTest.class,
	MainPageControllerTest.class,
	ResultPageControllerTest.class
	})
public class AllTests {

}
