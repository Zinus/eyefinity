package com.strahinja.java.web.eyefinity.services;


import java.io.File;
import java.io.IOException;

import org.junit.Test;

import junit.framework.TestCase;

public class FileReaderServiceTest extends TestCase{

	
	@Test
	public void testGetsNullshouldReturnNull() {
		FileReaderService fileReaderService = new FileReaderService();
		//null should return null
		try {
			assertNull(fileReaderService.readTxtFile(null));
		} catch (IOException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testGetsEmptyFileShouldReturnEmptyString() {
		File file = new File("src/test/resources/test_empty_string.txt");
		FileReaderService fileReaderService = new FileReaderService();
		//empty file should return empty string
		try {
			assertEquals("",fileReaderService.readTxtFile(file));
		} catch (IOException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testShouldReturnString() {
		File file1 = new File("src/test/resources/test_random_no_i.txt");
		FileReaderService fileReaderService = new FileReaderService();
		//random character string with line breaks
		try {
			assertEquals("abcdefghjk\r\nlmnopqrstu\r\n",fileReaderService.readTxtFile(file1));
		} catch (IOException e) {
			assertTrue(false);
		}
	}

}
