package com.strahinja.java.web.eyefinity.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ResultPageControllerTest {

	private MockMvc mvc;

	@Autowired
	ResultPageController resultPageController;
	
	@Autowired
	WebApplicationContext webApplicationContext;
	
	@Before
	public void setup() {
		 mvc = MockMvcBuilders.standaloneSetup(resultPageController)
	                .build();
	}
	
	@Test
	public void testShouldReturnNotNull() throws Exception {
	    assertThat(resultPageController).isNotNull();
	}
	
	@Test
	public void testShouldReturnStatusIsOk() {
		
		MockHttpServletResponse response;
		try {
			response = mvc.perform(
			        get("/result/").accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();
			assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		} catch (Exception e) {
			assertTrue(false);
		}
		
	}

}
